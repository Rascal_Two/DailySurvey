// Global state variables.
var state = {
  surveys: undefined,
  todaysSurvey: undefined,
  countDown: {
    date: undefined,
    interval: undefined
  }
}

/**
 * Add one day to a date.
 */
function appendDay(date){
  date.setDate(date.getDay() + 1);
}


/**
 * Show the clicked tab.
 *
 * @param {Event} event - Click event.
 * @param {String} tabName - Name of the tab to show.
 */
function showTab(event, tabName){
  var tabs = document.getElementsByClassName("menu");
  for (var i = 0; i < tabs.length; i++){
     tabs[i].style.display = "none";
  }
  var tabLinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tabs.length; i++){
     tabLinks[i].className = tabLinks[i].className.replace(" w3-red", "");
  }

  document.getElementById(tabName).style.display = "block";
  event.currentTarget.firstElementChild.className += " w3-red";
}


/**
 * Chart selected options into the provided element.
 *
 * @param {Array} options - Array of objects with 'answers' and 'text' keys.
 * @param {String} elementId - ID of element to put chart into.
 */
function chartOptions(options, elementId){
  var chart = AmCharts.makeChart(elementId, {
    "type": "pie",
    "theme": "none",
    "dataProvider": options,
    "valueField": "answers",
    "titleField": "text",
    "balloon":{
      "fixedPosition": true
    },
    "export": {
      "enabled": false
    }
  });
}


/**
 * Load an active survey.
 *
 * @param {Object} survey - Survey object.
 */
function loadActiveSurvey(survey){
  var div = document.getElementById("todays-survey");
  div.innerHTML = "";

  for (var i = 0; i < survey.questions.length; i++){
    var question = survey.questions[i];
    var questionElement = document.createElement("div");
    questionElement.className = "question-container";
    var header = document.createElement("h2");
    header.innerHTML = question.text;
    questionElement.appendChild(header);
    div.appendChild(questionElement);

    for (var j = 0; j < question.options.length; j++){
      var option = question.options[j];
      var optionElement = document.createElement("input");
      optionElement.type = "radio";
      optionElement.name = "option-" + question.id;
      optionElement.value = option.text;
      questionElement.appendChild(optionElement);

      var optionLabel = document.createElement("span");
      optionLabel.innerHTML = option.text;
      questionElement.appendChild(optionLabel);
      questionElement.appendChild(document.createElement("br"))
    }
  }
}


/**
 * Load a past survey.
 *
 * @param {Object} survey - Survey to load.
 */
function loadPastSurvey(survey){
  // TODO - Load a survey, showing every question, and a chart for every question
  // showing all the option counts.
  var chartDiv = document.createElement("div");
  chartDiv.className = "chart";
  chartDiv.id = "chart" + question.id;
  div.appendChild(chartDiv);

  chartOptions(question.options, "chart" + question.id);
}


/**
 * Render all surveys
 *
 * @param {Array} surveys - Array of surveys.
 */
function renderAllSurveys(surveys){
  // TODO - Render all provided surveys ordered by date into the
  // 'All' tab.
}


/**
 * Render popular surveys.
 *
 * @param {Array} surveys - Array of surveys.
 */
function renderPopularSurveys(surveys){
  // TODO - Render all surveys, ordered by the number of
  // answers they've gotten.
}


/**
 * Fetch todays survey, then load it.
 */
function fetchTodaysSurvey(){
  fetch("/api/survey/get/today").then(function(response){
    return response.json();
  }).then(function(survey){
    state.todaysSurvey = survey;
    loadActiveSurvey(state.todaysSurvey);
  }).catch(function(err){
    console.error(err);
  });
}


/**
 * Fetch all surveys, then render the poular and all lists.
 */
function fetchAllSurveys(){
  fetch("/api/survey/get/all").then(function(response){
    return response.json();
  }).then(function(surveys){
    state.surveys = surveys;
    renderAllSurveys(state.surveys);
    renderPopularSurveys(state.surveys);
  }).catch(function(err){
    console.error(err);
  })
}

// Code to be ran when the page is loaded.

state.countDown.date = new Date();
state.countDown.date.setHours(0, 0, 0, 0);
appendDay(state.countDown.date);


// Update the count down every 1 second
state.countDown.interval = setInterval(function(){
  var now = new Date().getTime();
  var distance = state.countDown.date - now;

  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById("countdown-timer").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";

  if (distance < 0){
    appendDay();
  }
}, 1000);

fetchTodaysSurvey();
fetchAllSurveys();