// This little dodad allows us to both see the same website.
// Basically, this 'tunnels' the runners 'localhost:3000' to 'rascaltwo.localtunnel.me'

// And with the current setup, it allows live reloading and such, so any changes made here
// only takes a few seconds to update on the localtunnel.

// Once we're both on and connected, I'll start it and you can get the idea.

var localtunnel = require("localtunnel");

var tunnel = localtunnel(3000, {subdomain: "rascaltwo"}, (err, client) => {
	if (err){
		console.log(err);
		return;
	}
	console.log("Opened tunnel to '" + client.url + "'");
});

tunnel.on("close", () => {
	console.log("Tunnel closed");
});

process.on("SIGINT", () => {
	console.log("\nCaught interrupt signal...");
	tunnel.close();
	setTimeout(process.exit, 1000);
});