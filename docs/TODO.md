# TODO

- Make some tests.
- Write some detailed documentation.
- Send todays survey to the client as a payload along with the inital HTML.
- Create a simple way to add surveys, either through the site or as a script.
- Move the Google Sheet information into a 'config.js' file.
- static/index.js
	- loadPastSurvey()
	- renderAllSurveys()
	- renderPopularSurveys()
	- Create actual error handlers for fetch*Surveys()
	- Move the code at the bottom into an actual function to be called when loading is finished.
- index.js
	- Add logging of HTTP requests.
	- /api/survey/answer
- database.js
	- Create method to insert and update rows.
	- getAnswers()