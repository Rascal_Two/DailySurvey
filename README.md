# Daily Surveys

A website that allows users to answer daily surveys and view past surveys.

## Files

- static/
	- Static image, Stylesheet, and JavaScript files.
- docs/
	- In-depth documentation.
- templates/
	- HTML files.
- database.js
	- Database reading, writing, and updating functions.
- index.js
	- Main server file.
- tunnel.js
	- Start a localtunnel for the server.

## Installation

`npm install`

`node start`

