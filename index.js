// If you haven't used the 'const' keyword yet, it's basically the same
// as 'var' except that you can't re-assign anything to it.

// So in Node.js, in order to use system libraries and such, you have to use
// 'require()', which you can read as Import.

// 'fs' is the FileSystem library, deals with reading, writing, and doing other
// file-based tasks.
const fs = require("fs");

//Function that asks for the IP of a USERPROFILE


// Gotta get and make the database.
var database = require("./database.js");
database = new database(
	"https://docs.google.com/spreadsheets/d/1AKF4rK7P5YrzOK810Bm4lt4VFphCWCvorwbwH2K5oQM/edit", {
	question: 0,
	survey: 817322285,
	option: 1294205709,
	answer: 1875399867
});

// Express is a framework that makes manading HTTP requests, responses, and and routing
// requests from the client easier.

// While we don't *need* express, we would be re-inventing a bunch of wheels without it,
// which while I have done so, there's no need to do that here.
const express = require("express");
const app = express();

// This makes it so that all requests to files - CSS files, JS files, images, etc
// will get said file from the 'static' folder.
app.use(express.static("static"));


// So this section is the Routes section. We'll probably only have a few routes,
// which would be the three I mentioned in the most recent reddit message.

// Whenever the client goes to "http://WHEREVER/", this will run.
app.get("/", (request, response) => {
  // '__dirname' is a global variable that is the full path of the current directory.
	response.sendFile(__dirname + "/templates/index.html");
});


app.post("/api/survey/answer", (request, response) => {
	var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
	// TODO - actually do this part.
});


app.get("/api/survey/get/today", (request, response) => {
	database.getTodaysSurvey(function(survey){
		response.json(survey);
	})
});


app.get("/api/survey/get/all", (request, response) => {
	database.getAll(function(surveys){
		response.json(surveys);
	})
});

// This last bit just starts the server, on port 3000.

app.listen(3000, (err) => {
	if (err){
		return console.log(err);
	}
	console.log("Server running on port 3000...");
});
