var sheetrock = require("sheetrock");

/**
 * Create a database instance.
 *
 * @param {String} sheetURL - URL of sheet.
 * @param {Object} sheetIDs - Object of sheet IDs.
 */
var Database = function(sheetURL, sheetIDs){
	this.sheetURL = sheetURL
	this.sheetIDs = sheetIDs
};


/**
 * Get all questions from the database.
 *
 * @param {Function} callback - Function to call with the questions when done.
 */
Database.prototype.getQuestions = function(callback){
	sheetrock({
		url: this.sheetURL + "#gid=" + this.sheetIDs.question,
		query: "select A,B,C",
		reset: true,
		callback: function(err, options, response){
			if (err){
				throw err;
			}
			var questions = [];
			for (var i = 1; i < response.rows.length; i++){
				var row = response.rows[i];
				questions.push({
					id: parseInt(row.cellsArray[0]),
					surveyId: parseInt(row.cellsArray[1]),
					text: row.cellsArray[2].trim()
				});
			}
			callback(questions);
		}
	});
};


/**
 * Get all surveys from the database.
 *
 * @param {Function} callback - Function to call with the surveys when done.
 */
Database.prototype.getSurveys = function(callback){
	sheetrock({
		url: this.sheetURL + "#gid=" + this.sheetIDs.survey,
		query: "select A,B,C",
		reset: true,
		callback: function(err, options, response){
			if (err){
				throw err;
			}
			var surveys = [];
			for (var i = 1; i < response.rows.length; i++){
				var row = response.rows[i];
				surveys.push({
					id: parseInt(row.cellsArray[0]),
					date: row.cellsArray[1],
					answers: parseInt(row.cellsArray[2])
				});
			}
			callback(surveys);
		}
	});
};


/**
 * Get all options from the database.
 *
 * @param {Function} callback - Function to call with the options when done.
 */
Database.prototype.getOptions = function(callback){
	sheetrock({
		url: this.sheetURL + "#gid=" + this.sheetIDs.option,
		query: "select A,B,C,D",
		reset: true,
		callback: function(err, options, response){
			if (err){
				throw err;
			}
			var options = [];
			for (var i = 1; i < response.rows.length; i++){
				var row = response.rows[i];
				options.push({
					id: parseInt(row.cellsArray[0]),
					questionId: parseInt(row.cellsArray[1]),
					text: row.cellsArray[2].trim(),
					answers: parseInt(row.cellsArray[3])
				});
			}
			callback(options);
		}
	});
};


/**
 * Get all answers from the database.
 *
 * @param {Function} callback - Function to call with the answers when done.
 */
Database.prototype.getAnswers = function(callback){
	// TODO - Create this function, returning all answers from the
	// database.
};

/**
 * Get all complete surveys from the database.
 *
 * @param {Function} callback - Function to call with the complete surveys when done.
 */
Database.prototype.getAll = function(callback){
	var data = {};
	this.getQuestions(function(questions){
		data.questions = questions;
	});
	this.getSurveys(function(surveys){
		data.surveys = surveys;
	});;
	this.getOptions(function(options){
		data.options = options;
	});
	// TODO - Add answers to surveys here.
	var waiting = setInterval(function(){
		if (!data.hasOwnProperty("questions") || !data.hasOwnProperty("surveys") || !data.hasOwnProperty("options")){
			return;
		}
		clearInterval(waiting);

		var surveys = [];
		for (var i = 0; i < data.surveys.length; i++){
			var survey = {
				id: data.surveys[i].id,
				date: data.surveys[i].date,
				answers: data.surveys[i].answers,
				questions: []
			};
			for (var j = 0; j < data.questions.length; j++){
				if (data.questions[j].surveyId !== survey.id){
					continue;
				}
				var question = {
					id: data.questions[j].id,
					text: data.questions[j].text,
					options: []
				};
				for (var k = 0; k < data.options.length; k++){
					if (data.options[k].questionId !== question.id){
						continue;
					}
					question.options.push({
						id: data.options[k].id,
						text: data.options[k].text,
						answers: data.options[k].answers
					});
				}
				survey.questions.push(question);
			}
			surveys.push(survey);
		}
		callback(surveys);
	}, 100);
};


/**
 * Get the survey for today, or null if there is none.
 *
 * @param {Function} callback - Function to call with the survey or null.
 */
Database.prototype.getTodaysSurvey = function(callback){
	var date = new Date();
	var month = date.getMonth() + 1;
	if (month.toString().length === 1){
		month = "0" + month;
	}
	var day = date.getDay();
	if (day.toString().length === 1){
		day = "0" + day;
	}
	date = new Date(date.getFullYear() + "-" + month + "-" + day);
	this.getAll(function(surveys){
		for (var i = 0; i < surveys.length; i++){
			if (new Date(surveys[i].date).getTime() !== date.getTime()){
				continue;
			}
			callback(surveys[i]);
			return;
		}
		callback(null);
	});
};

module.exports = Database;